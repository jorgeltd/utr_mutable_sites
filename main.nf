// Files
bed_sequencing_kit = file(params.bed_sequencing_kit)
utr5_bed = file(params.utr5_bed)
utr3_bed = file(params.utr3_bed)
ref_genome = file(params.ref_genome)
vcf_results = file(params.vcf_results)

// Intersect bed of sequencing kit with utrs5 and utr3 of genome
process bed_intersection{
  publishDir "${params.output_dir}/bed_intersection", mode: "copy"

  input:
  path utr5_bed
  path utr3_bed
  path bed_sequencing_kit

  output:
  path "*.bed" into bed_intersection_ch, bed_intersection_ch_2

  script:
  """
  bedtools intersect -a $bed_sequencing_kit -b $utr5_bed -u > "utr5_intersec.bed"
  bedtools intersect -a $bed_sequencing_kit -b $utr3_bed -u > "utr3_intersec.bed"	
  """
}

// Get utrs sequence covered by kit
process get_fasta_from_bed {
  publishDir "${params.output_dir}/fasta", mode: "copy"

  input:
  each path(bed_file) from bed_intersection_ch
  path ref_genome

  output:
  path "*.fasta" into kit_cov_fasta_ch

  script:
  name = 
  """
  bedtools getfasta -fi $ref_genome -bed $bed_file -name > "${bed_file.simpleName}.fasta"
  """
}


// Count possible sites on fasta that can mutate into AID pattern
process count_mutable_sites {
  publishDir "${params.output_dir}/mutable_sites_count", mode: "copy"

  input:
  each path(fasta_file) from kit_cov_fasta_ch

  output:
  path "*.txt"

  script:
  """
  count_mutable_sites.py -i $fasta_file -o ${fasta_file.simpleName}_mutable_sites_count.txt
  """
}


// Check if variants are covered by bed files
process check_bed_intersec {
  publishDir "${params.output_dir}/covered_variants", mode: "copy"

  input:
  path bed_files from bed_intersection_ch_2.collect()
  path vcf_results

  output:
  path "*.txt"
  path "*.csv"

  script:
  if ("${bed_files[0]}" =~ /utr5/ ) {
    utr3 = "${bed_files[1]}"
    utr5 = "${bed_files[0]}"
  }
  else{
    utr3 = "${bed_files[0]}"
    utr5 = "${bed_files[1]}"
  }
  """
  check_bed_intersec.py -d $vcf_results -5 $utr5 -3 $utr3 -o covered_variants
  """
}
