#!/usr/bin/env python

import argparse
import re
from Bio import SeqIO


"""
# AID patterns for 1-nucl context by side
aidp3 <- list(
  # Canonical AID signature should be C>T/G RCY
  RCY = c(f = c(m = "C-[T]", c = "[AG].[CT]"), r = c(m = "G-[A]", c = "[AG].[CT]")),
  # non-canonical according to Kasar A>C at WA
  WA = c(f = c(m = "A-[TGC]", c = "[AT].[ACGT]"), r = c(m = "T-[CGA]", c = "[ACGT].[AT]")),
  # signature 9 accordign to Alexandrov C>T at N.G
  RCG = c(f = c(m = "C-[T]", c = "[AG].G"), r = c(m = "G-[A]", c = "C.[CT]"))
)
"""


# AID PATTERNS
MUTABLE_SITES = {"RCY_FW": "[AG](C)[CT]",
                 "WA_FW": "[AT](A)[ACGT]",
                 "RCG_FW": "[AG](C)G",
                 "RCY_RV": "[AG](G)[CT]",
                 "WA_RV": "[ACGT](T)[AT]",
                 "RCG_RV": "C(G)[TC]"
                 }


def search_mutable_sites(seq):
    count = {p: 0 for p in MUTABLE_SITES.keys()}

    for p in MUTABLE_SITES.keys():
        count[p] += overlapping_matcher(MUTABLE_SITES[p], seq)

    return count


def overlapping_matcher(pattern, text):
    left = 0
    count = 0
    while True:
        match = re.search(pattern, text[left:])
        if not match:
            break
        count += 1
        left += match.start() + 1

    return count


def main():

    args = parse_arguments()
    mutable_sites_count = {key: 0 for key in MUTABLE_SITES.keys()}

    with open(args.input, "r") as handle:
        for record in SeqIO.parse(handle, "fasta"):
            count = search_mutable_sites(str(record.seq).upper())
            for key in count.keys():
                mutable_sites_count[key] += count[key]

    with open(args.output, 'w') as handle:
        total = 0
        handle.write(f"Pattern\tCount\n")
        for key in mutable_sites_count.keys():
            handle.write(f"{key}\t{mutable_sites_count[key]}\n")
            total += mutable_sites_count[key]
        handle.write(f"Total\t{total}\n")


def parse_arguments():
    parser = argparse.ArgumentParser(
        "Count mutable sites on multi fasta"
    )

    parser.add_argument(
        "-i",
        "--input",
        action="store",
        required=True,
        help="vcf like table",
    )

    parser.add_argument(
        "-o",
        "--output",
        action="store",
        required=True,
        help="output table with AID patterns annotated",
    )

    return parser.parse_args()


if __name__ == '__main__':
    main()


