#!/usr/bin/env python

import pandas as pd
import argparse


def check_position_in_bed(pos_gene, bed=None):
    pos = pos_gene['POS_bed']
    gene = pos_gene['Gene.ref.Gene']

    filter_bed = bed[bed['name'].str.contains(gene)]

    for i, row in filter_bed.iterrows():
        if pos in range(row['start'], row['end']):
            return True

    return False


def main():

    args = parse_arguments()
    df = pd.read_csv(args.data_file, skiprows=2, low_memory=False)
    header = ['chr', 'start', 'end', 'name']
    utr3 = pd.read_csv(args.utr3_file, sep='\t', names=header)
    utr5 = pd.read_csv(args.utr5_file, sep='\t', names=header)

    # BED FILES ARE 0-BASED INDEX
    # VCF FILES ARE 1-BASES INDEX
    # In this script VCF POSITIONS WILL BE SUBTRACTED by 1
    df['POS_bed'] = df['POS'] - 1

    df_utr3 = df[df['Func..refGene'] == 'UTR3'].copy()
    df_utr5 = df[df['Func..refGene'] == 'UTR5'].copy()

    df_utr3['Bed_Covered'] = df_utr3[['POS_bed', 'Gene.ref.Gene']].apply(check_position_in_bed, bed=utr3, axis=1)
    df_utr5['Bed_Covered'] = df_utr5[['POS_bed', 'Gene.ref.Gene']].apply(check_position_in_bed, bed=utr5, axis=1)

    pd.concat([df_utr3, df_utr5]).to_csv(args.output + '.csv', index=False)

    utr3_cov_bases = (utr3.end - utr3.start).sum()
    utr5_cov_bases = (utr5.end - utr5.start).sum()

    utr_3_in_bed = df_utr3[df_utr3['Bed_Covered'] == True].shape[0]
    utr_5_in_bed = df_utr5[df_utr5['Bed_Covered'] == True].shape[0]

    with open(args.output + '.txt', 'w') as f:
        f.write(f"UTR3 bases: {utr3_cov_bases}\n")
        f.write(f"UTR5 bases: {utr5_cov_bases}\n")
        f.write(f"UTR3 variants covered by bed: {utr_3_in_bed} of {df_utr3.shape[0]}\n")
        f.write(f"UTR5 variants covered by bed: {utr_5_in_bed} of {df_utr5.shape[0]}\n")


def parse_arguments():
    parser = argparse.ArgumentParser(
        "check if variants exist in bed files intersections"
    )

    parser.add_argument(
        "-d",
        "--data-file",
        action="store",
        required=True,
        help="vcf table results",
    )

    parser.add_argument(
        "-3",
        "--utr3-file",
        action="store",
        required=True,
        help="utr3 bed file with intersection of seq kit and utr3s",
    )

    parser.add_argument(
        "-5",
        "--utr5-file",
        action="store",
        required=True,
        help="utr5 bed file with intersection of seq kit and utr5s",
    )

    parser.add_argument(
        "-o",
        "--output",
        action="store",
        required=True,
        help="output",
    )

    return parser.parse_args()


if __name__ == '__main__':
    main()
