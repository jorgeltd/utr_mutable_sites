#!/usr/bin/env bash
sureselect="data/sureselect_V5+UTR/S04380219_Covered.bed"
sureselect_pad="data/sureselect_V5+UTR/S04380219_Padded.bed"
utr5="data/UTRs/utr5.bed"
utr3="data/UTRs/utr3.bed"
ref="/hd/data/genomes/by_chr/hg38.fa"
outdir="data"
fastadir="${outdir}/fasta"

mkdir -p $outdir $fastadir

bedtools intersect -a $sureselect -b $utr3 -u > "${outdir}/utr3_intersec.bed"
bedtools intersect -a $sureselect -b $utr5 -u > "${outdir}/utr5_intersec.bed"
bedtools intersect -b $sureselect -a $utr5 -u > "${outdir}/utr5_intersec.bed.tmp"

bedtools intersect -a $sureselect_pad -b $utr3 -u > "${outdir}/utr3_intersec_pad.bed"
bedtools intersect -a $sureselect_pad -b $utr5 -u > "${outdir}/utr5_intersec_pad.bed"

bedtools getfasta -fi $ref -bed "${outdir}/utr3_intersec.bed" -name > "${fastadir}/utr3.fasta"
bedtools getfasta -fi $ref -bed "${outdir}/utr5_intersec.bed" -name > "${fastadir}/utr5.fasta"

bedtools getfasta -fi $ref -bed "${outdir}/utr3_intersec_pad.bed" -name > "${fastadir}/utr3_pad.fasta"
bedtools getfasta -fi $ref -bed "${outdir}/utr5_intersec_pad.bed" -name > "${fastadir}/utr5_pad.fasta"
